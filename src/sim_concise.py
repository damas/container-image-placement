"""The simulation module that output a concise results.

    This module output the retrieval time per placement.
    i.e., the 'mean', 'median', '90th', '95th', '99th' and 'max'.
"""


import itertools

import numpy as np

from data_loader import load_layers, load_images, load_network_from_file
from dataframe import Dataframe
from model import Layer, Infrastructure
from placement import *
from retrieval_engine import RetrievalEngine


def run_expr(networks_file_id, retrieval_level, output_dir, config):
    """The main function of this module"""

    pid_str = 'run-%s-%s' % (networks_file_id, retrieval_level)
    print('start [%s] ..' % pid_str)

    csv_path = '%s/%s-level-%s.csv' % (output_dir, retrieval_level, networks_file_id)

    deterministic_placement_strategies = set([KcenterStrategy, KcenterWCStrategy])

    df = Dataframe(
        ('iter', 'network_id', 'nb_nodes', 'repf', 'agg_ds_size', 'cap_scaling',
         'placement', 'mean', 'median', '90th', '95th', '99th', 'max', 't_percent'))

    def persist_results():
        content = '\n'.join(df.to_csv()) + '\n'
        with open(csv_path, 'w') as fout:
            fout.write(content)

    def create_big_layer_t_kwargs(layers, big_layer_threshold_percent):
        inv_bltp = 1 - big_layer_threshold_percent
        sizes = sorted([l.size for l in layers])[int(inv_bltp * len(layers)):]
        return {'big_layer_threshold': min(sizes)}

    ######################################################

    dataset_dir = config['DATASET_DIR']
    networks_dir = config['NETWORKS_DIR']

    layers = load_layers(dataset_dir)
    # layers = [l for l in layers if l.size > 10*1024**2]
    # layers = [sorted(layers)[-1]]
    # for l in layers: l.size = 100 * 1024**2

    networks = load_network_from_file(networks_dir, networks_file_id)

    images = load_images(dataset_dir)

    ######################################################

    iterations = config['iterations']
    replication_factor_list = config['replication_factor_list']
    capacity_scaling_list = config['capacity_scaling_list']
    f_factor_list = config['f_factor_list']

    placement_strategies = [eval(p) for p in config['placement_strategies_list']]

    for expr_run in itertools.product(range(iterations),
                                      replication_factor_list,
                                      capacity_scaling_list,
                                      f_factor_list,
                                      list(networks.keys()),
                                      placement_strategies):

        itri, repf, cap_scaling, bltp, network_id, placement_class = expr_run

        # no more than one iteration for deterministic strategies
        if itri and placement_class in deterministic_placement_strategies:
            continue

        if placement_class != KcenterWCStrategy and bltp != 0.10:
            continue

        layers_rep = [Layer(l.id, repf, l.size) for l in layers]
        agg_ds_size = sum([l.size * l.nrep for l in layers_rep])

        bw_graph = networks[network_id]
        nb_nodes = len(bw_graph)

        if cap_scaling == -1: # infinite storage
            cap_scaling = nb_nodes

        node_cap = int(cap_scaling * agg_ds_size // nb_nodes)

        kwargs = create_big_layer_t_kwargs(layers, bltp)

        infra = Infrastructure(network_id, nb_nodes, bw_graph, node_cap)
        placement_strategy = placement_class(images, layers_rep, infra, **kwargs)

        print(placement_class.__name__, network_id, repf, cap_scaling)
        is_valid, placement = placement_strategy.place()

        if not is_valid:
            print("placement failed:", placement_class.__name__,
                  nb_nodes, network_id, repf, cap_scaling)
            df.add_row(itri, network_id, nb_nodes, repf, agg_ds_size, cap_scaling,
                       placement_class.__name__, -1, -1, -1, -1, -1, -1, bltp)
            continue

        re = RetrievalEngine(placement, infra)

        if retrieval_level == 'image':
            mp_layer_id = {l.id: l for l in layers_rep}
            rtimes = list()
            for node in range(nb_nodes):
                for imageid, im_layers_ids in images.iteritems():
                    im_layers = [mp_layer_id[layer_id]
                                 for layer_id in im_layers_ids]
                    _, irtime = re.retrieve_layers_to_node(im_layers, node)
                    rtimes.append(irtime)
        else:
            rtimes = re.all_retrieving_time(layers_rep)
            rtimes = sorted(rtimes)
            rtimes = [rt for rt in rtimes if rt > 0.01]

        df.add_row(itri, network_id, nb_nodes, repf, agg_ds_size, cap_scaling,
                   placement_class.__name__, np.mean(rtimes), np.median(rtimes),
                   np.percentile(rtimes, 90), np.percentile(rtimes, 95),
                   np.percentile(rtimes, 99), max(rtimes), bltp)

        # partial results in case of failure or long running simulation
        persist_results()


    persist_results()

    print('[%s] is done.' % pid_str)
