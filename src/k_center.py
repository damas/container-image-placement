"""The Src k-Center solver module
"""

from collections import namedtuple
import random


Edge = namedtuple('Edge', ['u', 'v', 'weight'])


def _dominant_set(capacities, edges, maxindex, size):
    """The Dominant Set algorithm called from the k-Center solver"""
    nb_nodes = len(capacities)
    cover_con = nb_nodes * [1]
    adj = [[]*nb_nodes for _ in range(nb_nodes)]
    for i in range(maxindex+1):
        e = edges[i]
        adj[e.u].append(e.v)
        adj[e.v].append(e.u)
        cover_con[e.u] += 1
        cover_con[e.v] += 1
    score = list(cover_con)
    number_visited = 0
    for u in range(nb_nodes):
        if capacities[u] < size:
            score[u] = float('inf')
            number_visited += 1
    dom_set = []
    while number_visited < nb_nodes:
        x = score.index(min(score))
        if cover_con[x] == 1 or [y for y in adj[x] if cover_con[y] == 1]:
            dom_set.append(x)
            cover_con[x] = 0
            for y in adj[x]:
                cover_con[y] = 0
        else:
            cover_con[x] -= 1
            for y in adj[x]:
                if cover_con[y] > 0:
                    cover_con[y] -= 1
                    score[y] += 1
        score[x] = float('inf')
        number_visited += 1
    return dom_set


def KCenterSrcSolver(capacities, bandwidth, size, k):
    """The k-Center solver"""
    if sum(1 for c in capacities if c >= size) < k:
        return []
    nb_nodes = len(capacities)
    if k == 1:
        i = 0
        while capacities[i] < size:
            i += 1
        index_chosen = i
        band_chosen = min(bandwidth[index_chosen])
        for i in range(index_chosen, nb_nodes):
            if capacities[i] >= size:
                band = min(bandwidth[i])
                if band > band_chosen:
                    index_chosen = i
                    band_chosen = band
        # index_max = max([i for i in xrange(nb_nodes) if capacities[i] >= size],
        #                 key=lambda i: min(bandwidth[i]))
        # if index_max != index_chosen:
        #     print(nb_nodes, index_max, index_chosen)
        capacities[index_chosen] -= size
        return [index_chosen]
    else:
        nb_nodes = len(bandwidth)
        edges = [Edge(u, v, bandwidth[u][v])
                 for u in range(nb_nodes)
                 for v in range(u+1, nb_nodes)]
        edges.sort(key=lambda edge: edge.weight, reverse=True)
        low, high = 0, len(edges) - 1
        while high >= low:
            mid = int((high+low)/2)
            selected = _dominant_set(capacities, edges, mid, size)
            if len(selected) <= k:
                high = mid-1
                centers = list(selected)
            else:
                low = mid+1
        # very rare, don't happen in practice
        if len(centers) < k:
            rem_nodes = [u for u in range(nb_nodes) if u not in centers]
            centers.extend(random.sample(rem_nodes, k-len(centers)))
        for j in centers:
            capacities[j] -= size
        return centers
