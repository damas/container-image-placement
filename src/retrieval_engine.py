"""The Retrieval Engine module.

    It computes the retrieval time of layers and images.
    i.e., the time needed to retreive an image or a layer to a node.
"""

class RetrievalEngine():
    """The Retrieval Engine implementation.

        The current implementation retreives a layer from its closest replica (
        i.e., the replica hosted on the node with the highest bandwidth to the
        destination node)

        NB: This heuristic does not guarantee the minimal retrieval time.
    """

    def __init__(self, placement, infrastructure):
        self.placement = placement
        self.infrastructure = infrastructure

    def retrieve_layer_to_node(self, layer, node):
        """Return the retrieval time of a layer and the index of the source replica"""
        if self.placement.is_in(layer, node):
            return 0, node
        bw_node = self.infrastructure.network[node]
        l_nodes = self.placement.get_nodes(layer)
        index_max = max(range(layer.nrep), key=lambda i: bw_node[l_nodes[i]])
        src_node = l_nodes[index_max]
        max_bw = bw_node[src_node]
        # layer size in byte and bw in bps
        min_rtime = 8.0 * layer.size / max_bw
        return min_rtime, src_node

    def all_retrieving_time(self, layers):
        """Return the retrieval times of a list of layers to all the nodes"""
        return [self.retrieve_layer_to_node(layer, node)[0]
                for node in self.infrastructure.nodes()
                for layer in layers]

    def retrieve_layers_to_node(self, layers, dest_node):
        """Return the retrieval time of a list of layers (image) to a node"""
        layers_requests = {}
        for layer in layers:
            if self.placement.is_in(layer, dest_node):
                continue
            _, src_node = self.retrieve_layer_to_node(layer, dest_node)
            layers_requests[src_node] = layers_requests.get(src_node, list()) + [layer]
        if not layers_requests:
            return list(), 0
        data_per_node = {node: sum(map(lambda l: l.size, layer))
                         for node, layer in layers_requests.items()}
        bw_node = self.infrastructure.network[dest_node]
        rtimes = [8.0 * dsize / bw_node[src_node]
                  for src_node, dsize in data_per_node.items()]
        return layers_requests, max(rtimes)
