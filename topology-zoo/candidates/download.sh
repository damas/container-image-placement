for link in \
Amres.gml \
Arn.gml \
Arnes.gml \
Bellsouth.gml \
Belnet2010.gml \
Biznet.gml \
Carnet.gml \
Cernet.gml \
Cesnet201006.gml \
Deltacom.gml \
Dfn.gml \
DialtelecomCz.gml \
Forthnet.gml \
Funet.gml \
Garr201201.gml \
Grnet.gml \
GtsCe.gml \
GtsPoland.gml \
GtsRomania.gml \
GtsSlovakia.gml \
Intranetwork.gml \
Ion.gml \
IowaStatewideFiberMap.gml \
Iris.gml \
Janetbackbone.gml \
Kdl.gml \
LambdaNet.gml \
Latnet.gml \
Litnet.gml \
Missouri.gml \
Niif.gml \
Ntelos.gml \
Palmetto.gml \
PionierL3.gml \
Rediris.gml \
Renater2010.gml \
RoedunetFibre.gml \
Sanet.gml \
Sinet.gml \
Sunet.gml \
Surfnet.gml \
SwitchL3.gml \
TataNld.gml \
Uninett2011.gml \
; do
  wget http://www.topology-zoo.org/files/$link;
done

mkdir with-link-speed
mv Amres.* Arnes.* Carnet.* Cernet.* Garr201201.* Grnet.* Litnet.* Niif.* Rediris.* Renater2010.* Sanet.* SwitchL3.* Uninett2011.* with-link-speed


for link in \
Cernet.gml \
Arnes.gml \
Garr201201.gml \
Grnet.gml \
Rediris.gml \
Uninett2011.gml \
; do
  sed -i '' '/graph \[/a \
\ \ multigraph 1\
' topology-zoo/candidates/with-link-speed/$link
done



sed -i '' 's:id "e9":LinkSpeedRaw 1000000000.0:g' \
  topology-zoo/candidates/with-link-speed/Garr201201.gml
sed -i '' 's:id "e25":LinkSpeedRaw 1000000000.0:g' \
  topology-zoo/candidates/with-link-speed/Garr201201.gml
sed -i '' 's:id "e60":LinkSpeedRaw 1000000000.0:g' \
  topology-zoo/candidates/with-link-speed/Garr201201.gml
sed -i '' '/description "Fibre ottica spenta (Dark Fibre)"/a \
\ \ \ \ LinkSpeedRaw 10000000000.0\
' topology-zoo/candidates/with-link-speed/Garr201201.gml

sed -i '' '/LinkLabel "10 GB\/s - redundantno"/a \
\ \ \ \ LinkSpeedRaw 10000000000.0\
' topology-zoo/candidates/with-link-speed/Arnes.gml

sed -i '' '/LinkLabel "2-34 Mbit\/s"/a \
\ \ \ \ LinkSpeedRaw 25000000.0\
' topology-zoo/candidates/with-link-speed/Uninett2011.gml

sed -i '' '/LinkLabel "100-155 Mbit\/s"/a \
\ \ \ \ LinkSpeedRaw 100000000.0\
' topology-zoo/candidates/with-link-speed/Uninett2011.gml

sed -i '' -e '1 s/Internal 1/Internal 0/; t' -e '1,// s//Internal 0/' \
  topology-zoo/candidates/with-link-speed/Uninett2011.gml

sed -i '' 's:source 0:source 1:g' \
  topology-zoo/candidates/with-link-speed/Uninett2011.gml

sed -i '' -e '1 s/GEANT/GEANT-0/; t' -e '1,// s//GEANT-0/' \
  topology-zoo/candidates/with-link-speed/Garr201201.gml

sed -i '' -e '1 s/GEANT/GEANT-0/; t' -e '1,// s//GEANT-0/' \
  topology-zoo/candidates/with-link-speed/Carnet.gml

sed -i '' -e '1 s/CERN/CERN-0/; t' -e '1,// s//CERN-0/' \
  topology-zoo/candidates/with-link-speed/SwitchL3.gml
sed -i '' -e '1 s/SwissIX/SwissIX-0/; t' -e '1,// s//SwissIX-0/' \
  topology-zoo/candidates/with-link-speed/SwitchL3.gml
sed -i '' -e '1 s/Swisscom/Swisscom-0/; t' -e '1,// s//Swisscom-0/' \
  topology-zoo/candidates/with-link-speed/SwitchL3.gml

sed -i '' -e '1 s/UiO/UiO-0/; t' -e '1,// s//UiO-0/' \
  topology-zoo/candidates/with-link-speed/Uninett2011.gml

sed -i '' -e '1 s/"UiTo"/"UiTo-0"/; t' -e '1,// s//"UiTo-0"/' \
  topology-zoo/candidates/with-link-speed/Uninett2011.gml
sed -i '' -e '1 s/"NORDUnet Stockholm"/"NORDUnet Stockholm-0"/; t' -e '1,// s//"NORDUnet Stockholm-0"/' \
  topology-zoo/candidates/with-link-speed/Uninett2011.gml

sed -i '' 's:id "e3":LinkSpeedRaw 1000000000.0:g' \
  topology-zoo/candidates/with-link-speed/litnet.gml

sed -i '' 's:target 22:target 12:g' \
  topology-zoo/candidates/with-link-speed/Cernet.gml

sed -i '' 's:LinkLabel "External":LinkSpeedRaw 1000000000.0:g' \
  topology-zoo/candidates/with-link-speed/Cernet.gml

sed -i '' 's:id "e50":LinkSpeedRaw 1000000000.0:g' \
  topology-zoo/candidates/with-link-speed/Cernet.gml


Remove by hand (Cernet.gml)
"""
node [
  id 22
  label "Shijiazhuang"
  Country "China"
  Longitude 114.47861
  Internal 1
  Latitude 38.04139
  type "small"
]
"""

# sed -i '' '0,/GEANT-xx/s/GEANT-xx/GEANT-ccc/' topology-zoo/candidates/with-link-speed/Garr201201.gml
# sed -i '' '0,/GEANT-xx/ s//GEANT-ccc/' topology-zoo/candidates/with-link-speed/Garr201201.gml
